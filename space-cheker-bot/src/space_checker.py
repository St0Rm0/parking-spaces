"""
Space_checker.py
"""
from typing import Dict
import time
from ast import literal_eval
import os
from user import User
import re
import requests
from telegram_bot_calendar import DetailedTelegramCalendar, LSTEP
from dotenv import load_dotenv
import telebot
from telebot import types, callback_data
from telebot.types import InlineKeyboardButton, InlineKeyboardMarkup
from bot_logger import get_logger
from manager import Manager, parse_range
from database import DataBase

LOGGER = get_logger(__name__)
WORK_DIR = "%s/.." % os.path.dirname(__file__)
DOTENV_PATH = os.path.join(WORK_DIR, 'space_checker.cfg')

if os.path.exists(DOTENV_PATH):
    load_dotenv(DOTENV_PATH)
else:
    LOGGER.critical('Config file "space_checker.cfg" not found')
    exit(1)

try:
    POSTGRES_PASSWORD = os.environ['POSTGRES_PASSWORD']
    POSTGRES_USER = os.environ['POSTGRES_USER']
    POSTGRES_HOST = os.environ['POSTGRES_HOST']
    POSTGRES_DB = os.environ['POSTGRES_DB']
    HISTORY_DIR = "%s/%s" % (WORK_DIR, os.environ['HISTORY_DIR'])
    TEST_FILE = os.environ['TEST_FILE']
    ADMINS = os.environ['ADMINS']
    TOKEN = os.environ['TOKEN']
    DIR = os.environ['DIR']
except KeyError as ex:
    LOGGER.critical('Key not found=%s', ex)
    exit(1)

global MANAGER
global BOT

BOT = telebot.TeleBot(TOKEN)

LIST_COMMANDS = [
    '/1',
    '/2',
    '/3',
    '/rqst',
    '/dirs',
    '/list',
    '/del',
    '/clear',
    '/cmds',
]


def help_user(message):
    """
    Вывод сообщения с основными командами для пользователей
    :param message:  telebot.types.Message
    """
    LOGGER.info('user_id=%s; print help', message.from_user.id)
    text = 'Список доступных команд:\n' \
           '/about  -  информация для связи с администраторами\n' \
           '/dir  -  запросить доступ к каталогам камер\n' \
           '/get  -  получить информацию об актуальном состоянии парковки'
    BOT.send_message(message.from_user.id, text)


def help_admin(message):
    """
    Вывод сообщения с основными командами для администраторов
    :param message: telebot.types.Message
    """
    LOGGER.info('user_id=%s; print help admin', message.from_user.id)
    text = 'Список доступных команд:\n' \
           '/rqst  - Список запросов доступа\n' \
           '/dirs  - Список всех директорий\n' \
           '/list  - Список пользователей и их прав\n' \
           '/del   - Отзыв прав у пользователя\n' \
           '/clear - Удалить все файлы кроме самого нового'
    BOT.send_message(message.from_user.id, text)


def send_analysis(list_files, user_id):
    if not list_files:
        BOT.send_message(user_id, 'Отсутствуют данные для анализа. Повторите попытку позже.')
        return

    for file in list_files:
        if re.fullmatch(r'[\S ]*.txt', file):
            LOGGER.info('send analysis document=%s', file)
            with open(file, 'rb') as doc:
                text_message = doc.read()
                BOT.send_message(user_id, text_message)
        elif re.fullmatch(r'[\S ]*.jpg', file):
            LOGGER.info('send histogram=%s', file)
            with open(file, 'rb') as photo:
                BOT.send_photo(user_id, photo, timeout=200)
        else:
            LOGGER.info('send analysis error=%s', file)
def select_dirs(message):
    """
    Выбор директорий, для отправки последних txt и jpg файлов
    :param message: telebot.types.Message
    """
    LOGGER.info('user_id=%s; Select dirs', message.from_user.id)

    data = message.text
    user_id = message.from_user.id
    list_files = MANAGER.get_files(data, user_id)

    if list_files is None:
        BOT.send_message(message.from_user.id, 'Неверные параметры')
        return

    if not list_files:
        BOT.send_message(message.from_user.id, 'Дирестори(я/и) пуст(а/ы)')
        return

    for file in list_files:
        if re.fullmatch(r'[\S ]*.txt', file):
            LOGGER.info('send document=%s', file)
            with open(file, 'rb') as doc:
                text_message = doc.read()
                BOT.send_message(message.from_user.id, text_message)
        elif re.fullmatch(r'[\S ]*.jpg', file):
            LOGGER.info('send photo=%s', file)
            with open(file, 'rb') as photo:
                callback_data_analysis = {
                    'user_id': message.from_user.id,
                    'command': 'analysis',
                    'folder': data
                }
                markup1 = types.InlineKeyboardMarkup()
                button1 = types.InlineKeyboardButton("Анализ парковочной зоны",
                                                     callback_data=str(callback_data_analysis))
                markup1.add(button1)
                BOT.send_photo(message.from_user.id, photo, timeout=200, reply_markup=markup1)
        else:
            LOGGER.info('send error=%s', file)


def print_cmds(message, user_id):
    """
    Вывод последний сообщений пользователя с user_id
    :param message: telebot.types.Message
    :param user_id: id пользователя
    """
    LOGGER.info('user_id=%s; Print cmds', message.from_user.id)

    if not str(message.text).isdigit():
        LOGGER.info('user_id=%s; Invalid parameters; message.text=%s',
                    message.from_user.id, message.text)
        return

    count = int(message.text)
    if count <= 0:
        text = "Вы ввеkи неправильное число!"
        BOT.send_message(message.from_user.id, text)
        return

    last_cmds = MANAGER.get_last_command(user_id, count)

    if last_cmds is None:
        text = "Произошла неизвестная ошибка"
        BOT.send_message(message.from_user.id, text)
        return

    text = ''

    for cmd in last_cmds:
        text += '%s\n' % cmd

    if not last_cmds:
        text = 'Список команд пользователя пуст'

    BOT.send_message(message.from_user.id, text)


def select_user(message, serial_nums):
    """
    Проверка выбранного пользователя
    :param message: telebot.types.Message
    :param serial_nums: словарь
        :key порядковый номер
        :var id пользователя
    """
    LOGGER.info('user_id=%s; Select user', message.from_user.id)

    if not str(message.text).isdigit():
        LOGGER.info('user_id=%s; Invalid parameters; message.text=%s',
                    message.from_user.id, message.text)
        return

    num = int(message.text)
    if num not in serial_nums:
        LOGGER.info('user_id=%s; Invalid parameters; num=%s not in serial_nums=%s',
                    message.from_user.id, num, serial_nums)
        return

    user_id = serial_nums[num]
    user = MANAGER.get_user(user_id)
    text = "Пользователь %s.\nВведите количество команд:" % str(user)

    BOT.send_message(message.from_user.id, text)
    BOT.register_next_step_handler(message, lambda msg: print_cmds(msg, user_id))


def select_clear_files(message):
    """
    Удаление файлов из директории, за исключением самого нового
    :param message: telebot.types.Message
    """
    LOGGER.info('user_id=%s; Select clear files', message.from_user.id)

    data = message.text
    list_dirs = MANAGER.get_directories()
    parse_data = parse_range(data, list_dirs)

    if parse_data is None:
        BOT.send_message(message.from_user.id, 'Неверные параметры')
        return

    MANAGER.clear_dirs(parse_data)
    BOT.send_message(message.from_user.id, 'Очищено')


def select_remove_rights_users(message, user_id):
    """
    Удаление прав и пользователя с user_id
    :param message: telebot.types.Message
    :param user_id: id пользователя
    """
    LOGGER.info('user_id=%s; Select remove rights', message.from_user.id)

    user = MANAGER.get_user(user_id)
    rights_user = list(user.get_rights().keys())
    list_ranges = parse_range(message.text, rights_user)

    if list_ranges is None:
        LOGGER.info('user_id=%s; Invalid parameters; list_range=%s',
                    message.from_user.id, list_ranges)

        BOT.send_message(
            message.from_user.id,
            "Неверные параметры")
        return

    MANAGER.remove_rights(user_id, list_ranges)
    BOT.send_message(
        message.from_user.id,
        'Доступ к директориям %s у пользователя %s отозваны' %
        (list_ranges, str(user)))

    BOT.send_message(
        user_id,
        'У вас отозвали доступ к директориям %s' % list_ranges)


def remove_rights_users(message, serial_nums):
    """
    Вывод двух кнопок с вариантами удаления прав у пользователя
    :param message: telebot.types.Message
    :param serial_nums: словарь
        :key порядковый номер
        :var id пользователя
    """
    LOGGER.info('user_id=%s; Remove rights users', message.from_user.id)

    if not str(message.text).isdigit():
        LOGGER.info('user_id=%s; Invalid parameters; message.text=%s',
                    message.from_user.id, message.text)

        BOT.send_message(message.from_user.id, 'Неверные параметры')
        return

    num = int(message.text)
    if num not in serial_nums:
        LOGGER.info('user_id=%s; Invalid parameters; num=%s not in serial_nums=%s',
                    message.from_user.id, num, serial_nums)

        BOT.send_message(message.from_user.id, 'Неверные параметры')
        return

    user = MANAGER.get_user(serial_nums[num])

    text = 'Пользователь %s \nДоступ к: %s\n\n' \
           'Отозвать права:' % (str(user), list(user.get_rights().keys()))

    callback_data_all = {
        'user_id': user.get_user_id(),
        'command': 'all_cru'
    }
    callback_data_select = {
        'user_id': user.get_user_id(),
        'command': 'select_cru'
    }

    inline_btn_1 = InlineKeyboardButton('Все', callback_data=str(callback_data_all))
    inline_btn_2 = InlineKeyboardButton('Выборочно', callback_data=str(callback_data_select))
    inline_kb = InlineKeyboardMarkup().add(inline_btn_1, inline_btn_2)

    BOT.send_message(message.from_user.id, text, reply_markup=inline_kb)


def change_reply_markup(origin: str, user_id: int):
    """
    Внедрение своих данный(id пользователя) в reply_markup
    :param origin: исходные данные
    :param user_id: id пользователя
    :return: Измененный reply_markup
    """
    LOGGER.info('user_id=%s; Create custom markup...', user_id)

    origin = literal_eval(origin)
    for line_data in origin["inline_keyboard"]:
        for button_data in line_data:
            button_data["callback_data"] += '_%s' % user_id

    LOGGER.info('user_id=%s; Created', user_id)
    return str(origin).replace('\'', '"')


def calendar(message, user_id: int):
    """
    Вывод календаря
    :param message: telebot.types.Message
    :param user_id: id пользователя
    """
    LOGGER.info('user_id=%s; Calendar, select date', message.from_user.id)

    markup, step = DetailedTelegramCalendar().build()
    custom_markup = change_reply_markup(markup, user_id)

    LOGGER.info('user_id=%s; Print calendar', message.from_user.id)

    BOT.send_message(
        message.chat.id,
        f"Select {LSTEP[step]}",
        reply_markup=custom_markup)


@BOT.callback_query_handler(func=DetailedTelegramCalendar.func())
def callback_calendar(call):
    """
    Обработуик нажатий кнопок в календаре
    :param call:
    """
    LOGGER.info('user_id=%s; Callback calendar', call.message.from_user.id)

    user_id = int(call.data.split('_')[-1])
    custom = '_'.join(call.data.split('_')[:-1])

    LOGGER.info('user_id=%s; Call: DetailedTelegramCalendar().process(custom)',
                call.message.from_user.id)

    result, key, step = DetailedTelegramCalendar().process(custom)
    if not result and key:
        LOGGER.info('user_id=%s; Callback calendar, next step', call.message.from_user.id)

        custom_key = change_reply_markup(key, user_id)
        BOT.edit_message_text(
            f"Select {LSTEP[step]}",
            call.message.chat.id,
            call.message.message_id,
            reply_markup=custom_key)
    elif result:
        LOGGER.info('user_id=%s; Print date=%s', call.message.from_user.id, result)

        BOT.edit_message_text(
            f"Дата истечения прав: {result}",
            call.message.chat.id,
            call.message.message_id)

        MANAGER.update_rights(user_id, result)
        request = MANAGER.get_request(user_id)

        if request is None:
            BOT.send_message(
                call.message.chat.id,
                'Другой администратор уже выдал права этому пользователю.')
            return

        BOT.send_message(
            request.get_user_id(),
            'Доступ к директориям %s разрешен' % request.get_dirs())

        LOGGER.info('user_id=%s; Remove messages from admins...', call.message.from_user.id)

        item = request.get_message_id()
        for i in item:
            LOGGER.info('user_id=%s; Remove message_id=%s, admin_id=%s',
                        call.message.from_user.id, i, item[i])
            BOT.delete_message(i, item[i])
        LOGGER.info('user_id=%s; Removed messages from admins - Success', call.message.from_user.id)

        MANAGER.remove_request(user_id)


def send_photos(message):
    """
    Функция отправления тестовый фото
    :param message: telebot.types.Message
    """
    LOGGER.info('user_id=%s; Send photos', message.from_user.id)
    files = MANAGER.get_test_files(message.text)

    if files is None:
        LOGGER.info('user_id=%s; Invalid parameters=%s', message.from_user.id, message.text)

        BOT.send_message(message.from_user.id, 'Неверные параметры')
        return

    for directory in files:
        if files[directory] is None:
            LOGGER.info('user_id=%s; No access, directory=%s', directory)

            text = 'Доступ к "%s" отсутствует' % directory
            BOT.send_message(message.from_user.id, text)
        else:
            LOGGER.info('user_id=%s; Send photo=%s', message.from_user.id, files[directory])
            try:
                with open(files[directory], 'rb') as photo:
                    BOT.send_photo(message.from_user.id, photo, directory, timeout=200)
            except requests.exceptions.ConnectionError as ex:
                LOGGER.error('user_id=%s; Send photos; %s', message.from_user.id, ex)


def request_rights(message):
    """
    Запрос прав пользователем
    :param message: telebot.types.Message
    """
    LOGGER.info('user_id=%s; Request rights', message.from_user.id)

    data = message.text
    is_dirs = MANAGER.is_dirs(data)

    if is_dirs is None:
        LOGGER.info('user_id=%s; Error parameters=%s', message.from_user.id, data)
        BOT.send_message(message.from_user.id, 'Произошла ошибка')
    elif not is_dirs:
        LOGGER.info('user_id=%s; Invalid parameters=%s', message.from_user.id, data)
        BOT.send_message(message.from_user.id, 'Неверные параметры')
    elif is_dirs:
        LOGGER.info('user_id=%s; Parameters=%s', message.from_user.id, data)
        dirs = MANAGER.parse_dirs(data)
        BOT.send_message(
            message.from_user.id,
            'Запрос на получение прав отправлен, ожидайте ответа')

        text = 'Пользователь %s запросил доступ к директорям "%s"\n' \
               'Разрешить доступ:' % (MANAGER.get_user(message.from_user.id), dirs)

        callback_data_all = {
            'user_id': message.from_user.id,
            'command': 'all_rr'
        }
        callback_data_select = {
            'user_id': message.from_user.id,
            'command': 'select_rr'
        }

        inline_btn_1 = InlineKeyboardButton('Ко всем', callback_data=str(callback_data_all))
        inline_btn_2 = InlineKeyboardButton('Выборочно', callback_data=str(callback_data_select))
        inline_kb = InlineKeyboardMarkup().add(inline_btn_1, inline_btn_2)

        message_id = {}
        admins = MANAGER.get_admins()

        LOGGER.info('user_id=%s; Remove old request', message.from_user.id)
        old_request = MANAGER.get_request(message.from_user.id)
        if old_request is not None:
            old_message_id = old_request.get_message_id()
            for admin_id in old_message_id:
                BOT.delete_message(admin_id, old_message_id[admin_id])
        LOGGER.info('user_id=%s; Removed old request', message.from_user.id)

        LOGGER.info('user_id=%s; Sending request to administrators', message.from_user.id)
        for admin in admins:
            LOGGER.info('user_id=%s; Sending request to=%s', message.from_user.id, admin)

            msg = BOT.send_message(admin.get_user_id(), text, reply_markup=inline_kb)
            message_id[admin.get_user_id()] = msg.message_id

        MANAGER.add_request(message.from_user.id, dirs, message_id)
    else:
        LOGGER.info('user_id=%s; Unknown error', message.from_user.id)
        BOT.send_message(message.from_user.id, 'Произошла неизвестная ошибка')


@BOT.callback_query_handler(func=lambda call: literal_eval(call.data)['command'] == 'analysis')
def callback_inline_first(call):
    user_id = literal_eval(call.data)['user_id']
    folder = literal_eval(call.data)['folder']
    LOGGER.info('user_id=%s; Get analysis, callback_data=%s',
                call.message.from_user.id, call.data)
    files = MANAGER.get_analysis_file(folder, user_id)
    send_analysis(files, user_id)



@BOT.callback_query_handler(func=lambda call: True)
def callback_button(call):
    """
    Функция обработки нажатий на кнопки выбора при удалении или выдаче прав
    :param call:
    """
    LOGGER.info('user_id=%s; Callback button', call.message.from_user.id)
    dict_data = literal_eval(call.data)
    try:
        user_id = dict_data['user_id']
        command = dict_data['command']
    except KeyError as error:
        LOGGER.error('user_id=%s; Error=%s', call.message.from_user.id, error)
        return
    if command == 'all_rr':
        LOGGER.info('user_id=%s; All rights, callback_data=%s',
                    call.message.from_user.id, command)

        calendar(call.message, user_id)
    elif command == 'select_rr':
        LOGGER.info('user_id=%s; Select rights, callback_data=%s',
                    call.message.from_user.id, command)

        BOT.reply_to(
            call.message,
            'Введите список директорий\n(пример запроса: 1,2,3-10,6):')

        BOT.register_next_step_handler(
            call.message,
            lambda message: select_rights(user_id, message))
    elif command == 'all_cru':
        LOGGER.info('user_id=%s; Remove all rights users, callback_data=%s',
                    call.message.from_user.id, command)

        user = MANAGER.get_user(user_id)
        MANAGER.remove_rights(user_id)
        BOT.edit_message_text(
            'Доступ к %s у пользователя %s отозван' %
            (list(user.get_rights().keys()), str(user)),
            call.message.chat.id,
            call.message.message_id)
    elif command == 'select_cru':
        LOGGER.info('user_id=%s; Select remove rights users, callback_data=%s',
                    call.message.from_user.id, command)

        user = MANAGER.get_user(user_id)
        text = 'Пользователь: %s\nПрава: %s\n\n' \
               'К каким директориям отозвать доступ:\n' \
               'пример(1,2,3-10,6):' % (str(user), list(user.get_rights().keys()))

        BOT.edit_message_text(
            text,
            call.message.chat.id,
            call.message.message_id)

        BOT.register_next_step_handler(
            call.message,
            lambda msg: select_remove_rights_users(msg, user_id))
    elif command == 'all_dirs':
        LOGGER.info('user_id=%s; Clear all directories, callback_data=%s',
                    call.message.from_user.id, command)

        dirs = MANAGER.get_directories()
        MANAGER.clear_dirs(dirs)
        BOT.edit_message_text('Очищено', call.message.chat.id, call.message.message_id)
    elif command == 'select_dirs':
        LOGGER.info('user_id=%s; Select clear directories, callback_data=%s',
                    call.message.from_user.id, command)

        dirs = MANAGER.get_directories()
        text = ''

        if not dirs:
            text = 'Директория пуста'
        else:
            text = 'Список директорий:\n'

        for directory in dirs:
            text += '%s\n' % directory

        if dirs:
            text += 'Введите список директорий\n(пример запроса: 1,2,3-10,6):'

        BOT.edit_message_text(
            text,
            call.message.chat.id,
            call.message.message_id)
        BOT.register_next_step_handler(call.message, select_clear_files)


def select_rights(user_id, message):
    """
    Выбор администратором прав пользователя
    :param user_id: id пользователя
    :param message: telebot.types.Message
    """
    LOGGER.info('user_id=%s; Select rights', message.from_user.id)

    allow = MANAGER.parse_dirs(message.text)
    request = MANAGER.get_request(user_id)

    if allow is None:
        LOGGER.info('user_id=%s; Invalid parameters=%s',
                    message.from_user.id, message.text)

        BOT.send_message(message.from_user.id, 'Неверные параметры')
        return
    if set(allow).issubset(request.get_dirs()):
        LOGGER.info('user_id=%s; Rights correct=%s',
                    message.from_user.id, message.text)

        MANAGER.set_request_dirs(user_id, allow)
        calendar(message, user_id)
    else:
        LOGGER.info('user_id=%s; Invalid parameters=%s' %
                    (message.from_user.id, message.text))

        BOT.send_message(message.from_user.id, 'Неверные параметры')


@BOT.message_handler(commands=['start'])
def start(message):
    """
    Функция обработки команды /start
    :param message: telebot.types.Message
    """

    LOGGER.info('User=%s; Command=%s', message.from_user.first_name, message.text)
    if not MANAGER.is_user(message.from_user.id):
        user = User(
            message.from_user.id,
            message.chat.id,
            message.from_user.first_name,
            message.from_user.username)

        log_file_name = "%s.txt" % user.get_user_id()
        log_file = "%s/%s" % (HISTORY_DIR, log_file_name)
        user.set_history_file(log_file)

        MANAGER.add_user(user)

        BOT.send_message(message.from_user.id, f'Добро пожаловать, {message.from_user.first_name}!')
    if MANAGER.is_admin(message.from_user.id):
        text = 'Добро пожаловать!\nВы администратор.\n' \
               'Введите /help для просмотра всех доступных команд'
        BOT.send_message(message.from_user.id, text)
    else:
        text = 'Бот позволяет получать информацию ' \
               'о наличии свободных мест ' \
               'на автостоянке. \n \nДля того, чтобы начать получать информацию ' \
               'необходимо запросить у администраторов разрешение на доступ ' \
               'к директориям определенных камер. ' \
               'Информация о решении администраторов будет отправлена Вам в виде сообщения \n \n' \
               'Использовать функции бота можно либо с помощью меню, ' \
               'либо непосредственным вводом команд\n'
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
        choice_item = types.KeyboardButton('📁\n Выбор директорий')
        search_item = types.KeyboardButton('🅿️\n Просмотр парковки')
        help_item = types.KeyboardButton('🆘\n Помощь')
        markup.add(choice_item, search_item)
        markup.add(help_item)

        BOT.send_message(message.from_user.id, text,
                         reply_markup=markup)
        help_user(message)


@BOT.message_handler(content_types=['text'])
def get_text_messages(message):
    """
    Функция обработки всех команд, кроме /start
    :param message: telebot.types.Message
    """
    LOGGER.info('User=%s; Command=%s', message.from_user.first_name, message.text)

    if message.text in LIST_COMMANDS:
        MANAGER.log_command(message.from_user.id, message.text)

    if MANAGER.is_admin(message.from_user.id):
        LOGGER.info('User=%s; is admin', message.from_user.first_name)

        if message.text == '/help':
            help_admin(message)
        elif message.text == '/dirs':
            text = ''
            contents = MANAGER.get_directories()

            if contents is None:
                BOT.send_message(message.from_user.id, 'Произошла ошибка')
                return
            if not contents:
                BOT.send_message(message.from_user.id, 'Директория пуста')
                return
            for directory in contents:
                text += '%s\n' % directory

            BOT.send_message(message.from_user.id, text)
        elif message.text == '/list':
            text = 'Ниже представлен список пользователей.\n' \
                   'Имя пользователя (@username) [Список директорий]\n'
            users = MANAGER.get_users()
            for user in users:
                if user.get_status() == 'admin':
                    text += '%s - admin\n\n' % repr(user)
                else:
                    text += '%s\n\n' % repr(user)

            BOT.send_message(message.from_user.id, text)
        elif message.text == '/rqst':
            rqsts = MANAGER.get_requests()
            text = ''
            if not rqsts:
                text = 'Список запросов доступа пуст'
            for item in rqsts:
                user = MANAGER.get_user(item.get_user_id())
                text += '%s: запросил доступ к: %s\n\n' % (str(user), item.get_dirs())

            BOT.send_message(message.from_user.id, text)
        elif message.text == '/del':
            serial_nums: Dict[int, int] = {}
            text = ''
            num = 1
            for user in MANAGER.get_users():
                if user.get_status() == 'admin':
                    continue

                text += '%s) %s\n\n' % (num, repr(user))

                serial_nums[num] = user.get_user_id()
                num += 1

            if not text:
                text = 'Пользователи отсутствуют'
                BOT.send_message(message.from_user.id, text)
            else:
                BOT.send_message(message.from_user.id, text)
                BOT.send_message(message.from_user.id, 'Введите порядковый номер пользователя:')
                BOT.register_next_step_handler(
                    message,
                    lambda msg: remove_rights_users(msg, serial_nums))
        elif message.text == '/clear':
            text = 'Удалить все файлы из конкретной папки за исключением самого нового'

            callback_data_all = {
                'user_id': 0,
                'command': 'all_dirs'
            }
            callback_data_select = {
                'user_id': 0,
                'command': 'select_dirs'
            }

            inline_btn_1 = InlineKeyboardButton(
                'Во всех',
                callback_data=str(callback_data_all))
            inline_btn_2 = InlineKeyboardButton(
                'Выборочно',
                callback_data=str(callback_data_select))
            inline_kb = InlineKeyboardMarkup().add(inline_btn_1, inline_btn_2)

            BOT.send_message(message.from_user.id, text, reply_markup=inline_kb)
        elif message.text == '/cmds':
            serial_nums: Dict[int, int] = {}
            text = ''
            num = 1
            for user in MANAGER.get_users():
                if user.get_status() == 'admin':
                    continue

                text += '%s) %s\n\n' % (num, repr(user))

                serial_nums[num] = user.get_user_id()
                num += 1

            if not text:
                text = 'Пользователи отсутствуют'
                BOT.send_message(message.from_user.id, text)
            else:
                BOT.send_message(message.from_user.id, text)
                BOT.send_message(message.from_user.id, 'Введите порядковый номер пользователя:')
                BOT.register_next_step_handler(
                    message,
                    lambda msg: select_user(msg, serial_nums))
        else:
            BOT.send_message(message.from_user.id, 'Неизвестная команда')
            help_admin(message)
    else:
        LOGGER.info('User=%s; is user', message.from_user.first_name)

        if message.text == '/help' or message.text == '🆘\n Помощь':
            help_user(message)

        elif message.text == '/about':
            text = 'Для связи с разработчиками ' \
                   'просьба писать в телеграмм ' \
                   '@git_st0rm'
            BOT.send_message(message.from_user.id, text)

        elif message.text == '/dir' or message.text == '📁\n Выбор директорий':
            contents = MANAGER.get_directories()

            if contents is None:
                text = 'Произошла ошибка'
                BOT.send_message(message.from_user.id, text)
                return
            if not contents:
                text = 'Директория пуста'
                BOT.send_message(message.from_user.id, text)
                return

            inline_kb = InlineKeyboardMarkup(row_width=3)

            text = 'Список директорий:\n'
            for directory in contents:
                text += '{}\n'.format(directory)

            BOT.send_message(message.from_user.id, text)
            BOT.send_message(
                message.from_user.id,
                'Введите список директорий, '
                'к которым Вы хотите получить доступ:\n'
                '(пример запроса: 1,2 или 3-10)')

            BOT.register_next_step_handler(message, request_rights)
        elif message.text == '/2':
            contents = MANAGER.get_directories()

            if contents is None:
                BOT.send_message(message.from_user.id, 'Произошла ошибка')
                return
            if not contents:
                BOT.send_message(message.from_user.id, 'Директория пуста')
                return

            text = 'Список директорий:\n'
            for folder in contents:
                text += '{}\n'.format(folder)

            BOT.send_message(message.from_user.id, text)
            BOT.send_message(
                message.from_user.id,
                'Введите список директорий,'
                'тестовый фото которой хотите получить\n'
                '(пример запроса: 1,2,3-10,6):')

            BOT.register_next_step_handler(message, send_photos)
        elif message.text == '/get' or message.text == '🅿️\n Просмотр парковки':
            contents = MANAGER.get_directories(message.from_user.id)

            if contents is None:
                BOT.send_message(message.from_user.id, 'Произошла ошибка')
                return
            if not contents:
                BOT.send_message(message.from_user.id, 'Директория пуста')
                return

            text = 'Список директорий:\n'
            for folder in contents:
                text += '{}\n'.format(folder)

            BOT.send_message(message.from_user.id, text)
            BOT.send_message(
                message.from_user.id,
                'Введите список директорий, '
                'информацию из которых Вы хотите получить:\n'
                '(пример запроса: 1,2 или 3-10)')
            BOT.register_next_step_handler(message, select_dirs)
        else:
            BOT.send_message(message.from_user.id, 'Неизвестная команда')
            help_user(message)


if __name__ == '__main__':
    time.sleep(5)

    DB = DataBase(
        pswd=POSTGRES_PASSWORD,
        db_name=POSTGRES_DB,
        host=POSTGRES_HOST,
        user=POSTGRES_USER)

    FOLDER = os.path.join(os.path.dirname(__file__), '../', DIR)

    if not os.path.isdir(FOLDER):
        LOGGER.critical('Directory "%s" not found', FOLDER)
        exit(1)

    if not os.path.isdir(HISTORY_DIR):
        os.makedirs(HISTORY_DIR)

    MANAGER = Manager(DB, HISTORY_DIR)
    MANAGER.set_folder(FOLDER)
    MANAGER.set_test_file(TEST_FILE)
    MANAGER.set_list_admins(ADMINS)

    LOGGER.info('start BOT')
    BOT.polling(none_stop=True, interval=0)
