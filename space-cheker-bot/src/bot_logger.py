"""
bot_logger.py
"""
import logging
import logging.config

LOG_FILE = '../space_checker.log'
LOG_FORMAT = '[%(asctime)s]-[%(levelname)s]-[%(name)s]>>%(message)s'


def get_file_handler():
    """
    Создание обработчика вывода лога в файл
    :return: обработчик вывода в файл
    """
    file_handler = logging.FileHandler(LOG_FILE)
    file_handler.setLevel(logging.DEBUG)
    file_handler.setFormatter(logging.Formatter(LOG_FORMAT))
    return file_handler


def get_stream_handler():
    """
    Создание обработчика лога в поток вывода
    :return: обработчик потока вывода
    """
    stream_handler = logging.StreamHandler()
    stream_handler.setLevel(logging.DEBUG)
    stream_handler.setFormatter(logging.Formatter(LOG_FORMAT))
    return stream_handler


def get_logger(name):
    """
    СОздание логгера
    :param name: имя файла
    :return: логгер
    """
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)
    logger.addHandler(get_file_handler())
    logger.addHandler(get_stream_handler())
    return logger
