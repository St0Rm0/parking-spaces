"""
Модуль manager
This exports:
 - Item
 - Manager
"""
import os
from typing import List, Dict
from user import User
import re
import datetime
from database import DataBase
from bot_logger import get_logger

LOGGER = get_logger(__name__)


def parse_range(data: str, model: List[str]) -> List[str]:
    """
    Преодразование строки в список
    :param data: строка вида: 1,2,3-8,10,34-100
    :param model: список с которым будет производится сравнение
    example:
        data = '1,2,3,4-7,10,
        model = ['1','2','3','4','6','7','8','9,'10']
        return: ['1','2','3','4','6','7','10']

    :return: список элементов присутствующих в model
    """
    LOGGER.debug('Parse range; data=%s, model=%s', data, model)
    if not isinstance(model, list):
        return None

    if not model:
        return []

    set_range = set()
    list_data = data.replace(' ', '').split(',')
    for rng in list_data:
        try:
            beg_dir, end_dir = rng.split('-')
        except ValueError:
            beg_dir = end_dir = rng

        LOGGER.debug('Parse range; beg_dir=%s, end_dir=%s', beg_dir, end_dir)

        if (beg_dir not in model) or (end_dir not in model):
            LOGGER.debug('Parse range; Invalid data="%s"', data)
            return None

        beg_dir_index = model.index(beg_dir)
        end_dir_index = model.index(end_dir) + 1
        if end_dir_index <= beg_dir_index:
            LOGGER.debug('Parse range; Invalid data="%s"', data)
            return None
        for cur_dir in model[beg_dir_index:end_dir_index]:
            set_range.add(cur_dir)
    LOGGER.debug('Parse range; Directory parsing successfull: %s', set_range)

    list_range = list(set_range)
    list_range.sort()
    LOGGER.debug('Parse range; return=%s', list_range)
    return list_range


class Item:
    """
    Хранит все запросы доступа пользователя к директориям
    """

    def __init__(self, user_id: int, dirs: List[str], message_id: Dict[int, int]):
        """
        Конструктор
        :param user_id: id пользователя отправившего запрос
        :param dirs: список директорий, к которым пользователь желает получить доступ
        :param message_id: нужен для удаления у всех администраторов сообщения о запросе доступа
            :key id администратора
            :var id сообщения запроса доступа пользователя с user_id
        """
        LOGGER.debug('Create Item; user_id=%s, dirs=%s, message_id=%s',
                     user_id, dirs, message_id)

        self._user_id = user_id
        self._dirs = dirs
        self._message_id = message_id

    def get_user_id(self) -> int:
        """
        Метод получения id пользователя
        :return: id пользователя
        """
        LOGGER.debug('Item; get_user_id, return=%s', self._user_id)
        return self._user_id

    def get_dirs(self) -> List[str]:
        """
        Медот получения списка директорий
        :return: список директорий, к которы пользователь с user_id желает получить доступ
        """
        LOGGER.debug('Item; get_dirs, return=%s', self._dirs)
        return self._dirs

    def get_message_id(self) -> Dict[int, int]:
        """
        Метод получения id сообщений
        :return: словарь
            :key id администратора
            :var id сообщения запроса доступа пользователя с user_id
        """
        LOGGER.debug('Item; get_message_id, return=%s', self._message_id)
        return self._message_id

    def set_dirs(self, dirs: List[str]) -> None:
        """
        Метод, устанавливающий список директорий
        :param dirs: список директорий
        """
        LOGGER.debug('Item; set_dirs, dirs=%s', dirs)
        self._dirs = dirs

    def __eq__(self, other):
        """
        Сравнение на равенство запросов
        (для тестирования)
        :param other: объект класса Item
        :return:
            True: если оба объекта равны по данным
            False: в противном случае
        """
        return self._dirs == other.get_dirs() and \
            self._user_id == other.get_user_id() and \
            self._message_id == other.get_message_id()


class Manager:
    """
    Класс - Менеджер управления, взаимодействия и хранения
    пользователей telegram бота
    """

    def __init__(self, db: DataBase, history_dir: str = None):
        """
        Конструктор класс
        :param db: база данных
        """
        LOGGER.info('Create manager; db=%s', db)
        self._db = db
        self._users: Dict[int, User] = \
            {user.get_user_id(): user for user in db.get_users_list()}
        self._folder = ''
        self._requests: Dict[int, Item] = {}
        self._test_file = 'test_image.jpg'
        self._list_admins: List[str] = []
        self._history_dir = "history" if history_dir is None else history_dir
        self._init_history_files()

    def get_users(self) -> List[User]:
        """
        Метод получения списка пользователей бота
        :return: список пользователей
        """
        LOGGER.debug('Get list users')
        return [user for user in self._users.values()]

    def is_user(self, user_id: int) -> bool:
        """
        Метод проверки пользователя по user_id (status не важен)
        :param user_id: id пользователя
        :return:
            True: Если существует пользователя с user_id
            False: Если не существует пользователя с user_id
        """
        LOGGER.debug('Is user; user_id=%s', user_id)
        return user_id in self._users

    def is_admin(self, user_id: int) -> bool:
        """
        Метод проверки пользователя по user_id (проверка по status)
        :param user_id: id пользователя
        :return:
            True: Если пользователь с user_id имеет статус 'admin'
            False: Если пользователь с user_id не имеет статус 'admin' или
                   такого пользователя не существует
        """
        LOGGER.debug('Is admin; user_id=%s', user_id)
        return user_id in [u.get_user_id() for u in self._users.values()
                           if u.get_status() == 'admin']

    def add_user(self, user: User) -> None:
        """
        Метод добавления пользователя
        :param user: пользователя
        """
        LOGGER.debug('Add user; user=%s', user)

        if user.get_username() in self._list_admins:
            user.set_status('admin')

        self._users[user.get_user_id()] = user
        self._db.insert(user)

    def set_folder(self, path: str) -> None:
        """
        Метод установки директории
        :param path: абсолютный или относителяный путь к директории
        """
        LOGGER.debug('Set folder; path=%s', path)
        self._folder = path

    def set_test_file(self, file: str) -> None:
        """
        Метод установки тестового файла для отправки пользователям
        :param file: названия файла (без относительного или абсолютного пути)
        """
        LOGGER.debug('Set test file; file=%s', file)
        self._test_file = file

    def get_directories(self, user_id: int = None) -> List[str]:
        """
        Метод получения списка директорий в папке self._folder
        :param user_id: id пользователя
        :return: Если не указан id пользователя, то возвращается весь список файлов
                 в директории self._folder
                 Если id пльзователя указан, то возвращается список только разрешенных
                 данному пользователю файлов
        """
        LOGGER.debug('get_directories; user_id=%s', user_id)
        if not os.path.isdir(self._folder):
            LOGGER.error('Directory "%s" not found', self._folder)
            return None

        list_dir = os.listdir(self._folder)
        list_dir.sort()
        contents = []

        if user_id is None:
            LOGGER.debug('User_id is None')
            contents = list_dir
        elif user_id in self._users:
            LOGGER.debug('User found: %s', self._users[user_id])
            user = self._users[user_id]
            rights = user.get_rights()
            today = datetime.date.today()

            rights_ended = []
            contents = []
            for directory in list_dir:
                if directory not in rights:
                    continue

                date_rights = rights[directory]
                if date_rights >= today:
                    contents.append(directory)
                else:
                    rights_ended.append(directory)

            self.remove_rights(user_id, rights_ended)
        else:
            LOGGER.error('User not found, user_id: %s', user_id)
            contents = None
        LOGGER.debug('Return directories: %s', contents)
        return contents

    def parse_dirs(self, data: str, user_id: int = None) -> List[str]:
        """
        Преобразование запроса вида: '1-4,5,7,10-67'
        в список директорий
        :param data: строка вида: '1-4,5,7,10-67'
        :param user_id: id пользователя
        :return:
            None: Если диресктории self._folder не существует или
                  строка data неправильного формата
            List[str]: Если user_id is None,
                       возвращается список всех директорий.
                       Если user_id not is None,
                       возвращается список только разрешенных
                       данному пользователю Директорий
        """
        LOGGER.debug('Parsing directories; data=%s, user_id=%s', data, user_id)
        if not os.path.isdir(self._folder):
            LOGGER.error('Directory "%s" not found', self._folder)
            return None

        exist_dirs = []
        if user_id is None:
            LOGGER.debug('user_id is None, user_id=%s', user_id)
            exist_dirs = os.listdir(self._folder)
            exist_dirs.sort()
        elif user_id in self._users:
            LOGGER.debug('user_id found, user_id=%s', user_id)

            today = datetime.date.today()
            rights = self._users[user_id].get_rights()
            for folder in rights:
                current_date = datetime.datetime.strptime(str(rights[folder]), '%Y-%m-%d')
                if current_date.date() >= today:
                    exist_dirs.append(folder)

            exist_dirs.sort()
        else:
            LOGGER.debug('user_id not found, user_id=%s', user_id)
            exist_dirs = None

        LOGGER.debug('return=parse_range()')
        return parse_range(data, exist_dirs)

    def get_files(self, data: str, user_id) -> List[str]:
        """
        Получение списка последних jpg и txt файлов, из указанных директорий
        :param data: строка вида: '1-4,5,7,10-67'
        :param user_id: id пользователя
        :return: список последних jpg и txt файлов
        """
        LOGGER.debug('Get files; data=%s, user_id=%s', data, user_id)
        folders = self.parse_dirs(data) if user_id is None else self.parse_dirs(data, user_id)
        if folders is None:
            return None

        ret_files = []
        format_files = ['txt', 'jpg']
        format_date = '%Y.%m.%d %H.%M.%S'
        base_pattern = r'\d{4}.\d{2}.\d{2}.\d{2}.\d{2}.\d{2}'
        list_pattern = ['%s.%s' % (base_pattern, ff) for ff in format_files]
        list_path_dirs = ['%s/%s' % (self._folder, folder) for folder in folders]

        for folder in list_path_dirs:

            if not os.path.isdir(folder):
                continue

            for pattern in list_pattern:
                list_files = []

                for file in os.listdir(folder):
                    if re.fullmatch(pattern, file):
                        list_files.append(file)

                if not list_files:
                    break

                list_date = []
                for file in list_files:
                    list_date.append(datetime.datetime.strptime(
                        '.'.join(file.split('.')[:-1]),
                        format_date))

                list_date.sort()
                date_str = list_date[-1].strftime(format_date)

                for file in list_files:
                    if file.startswith(date_str):
                        ret_files.append('%s/%s' % (folder, file))
                        break
                    if file.endswith('histogram.jpg'):
                        ret_files.append('%s/%s' % (folder, file))
                        break
        return ret_files

    def get_test_files(self, data: str, user_id: int = None) -> Dict[str, str]:
        """
        Метод получения словаря тестовых файлов
        :param data: строка вида: '1-4,5,7,10-67'
        :param user_id: id пользователя
        :return:
            None: Если диресктории self._folder не существует или
                  строка data неправильного формата
            Dict[str, str]: Если user_id is None,
                возвращается словарь всех директорий.
                Если user_id not is None,
                возвращается словарь только разрешенных
                данному пользователю Директорий
                :key название директории
                :var абсолютный путь к тестовому вайлу в директории key
        """
        LOGGER.debug('Get test files; data=%s, user_id=%s', data, user_id)
        folders = self.parse_dirs(data) if user_id is None else self.parse_dirs(data, user_id)
        if folders is None:
            return None

        files = {}
        if user_id is None:
            LOGGER.debug('user_id is None')
            files = {d: '%s/%s/%s' % (self._folder, d, self._test_file) for d in folders}
        elif user_id in self._users:
            LOGGER.debug('User found: %s', self._users[user_id])
            for folder in folders:
                files[folder] = '%s/%s/%s' % (self._folder, folder, self._test_file)

        LOGGER.debug('Files: %s', files)
        return files

    def get_analysis_file(self, data: str, user_id) -> List[str]:
        LOGGER.debug('Get analysis; data=%s, user_id=%s', data, user_id)
        folders = self.parse_dirs(data) if user_id is None else self.parse_dirs(data, user_id)
        if folders is None:
            return None

        ret_files = []
        list_path_dirs = ['%s/%s' % (self._folder, folder) for folder in folders]

        for folder in list_path_dirs:

            if not os.path.isdir(folder):
                continue
            for file in os.listdir(folder):
                if file.endswith("histogram.jpg") or file.endswith("histogram.txt"):
                    ret_files.append('%s/%s' % (folder, file))
        return ret_files

    def is_dirs(self, data: str) -> bool:
        """
        Проверка на существование директорий
        :param data: строка вида: '1-4,5,7,10-67'
        :return:
            True: Если указанные директории существуют
            False: Если строка введена непровильно или
                   указанных директорий не существует
        """
        LOGGER.debug('Is dirs; data=%s', data)
        return self.parse_dirs(data) is not None

    def add_request(self, user_id: int, dirs: List[str], message_id: Dict[int, int]) -> None:
        """
        Добавление запроса от поьзователя
        :param user_id: id пользователя
        :param dirs: список директорий
        :param message_id: словарь id сообщений
            :key id администратора
            :var id сообщения-запроса
        """
        LOGGER.debug('Add request; user_id=%s, directories=%s, message_id=%s',
                     user_id, dirs, message_id)
        self._requests[user_id] = Item(user_id, dirs, message_id)

    def set_request_dirs(self, user_id: int, dirs: List[str]) -> None:
        """
        Изменение списка запрошенных директорий пользователем с user_id
        :param user_id: id пользователя
        :param dirs: список директорий
        """
        LOGGER.debug('Set request directories; user_id=%s, directories=%s', user_id, dirs)
        item = self.get_request(user_id)
        if item is not None:
            item.set_dirs(dirs)

    def get_requests(self) -> List[Item]:
        """
        Получение списка всех запросов от пользователей
        :return: список запросов
        """
        LOGGER.debug('Get requests')
        return [r for r in self._requests.values()]

    def get_request(self, user_id: int) -> Item:
        """
        Получение списка запроса от пользователя с user_id
        :param user_id: id пользователя
        :return:
            None: Если запрос от пользователя с user_id не существует
            Item: Если запрос от пользователя с user_id существует
        """
        LOGGER.debug('Get requests; user_id=%s', user_id)
        if user_id in self._requests:
            return self._requests[user_id]
        return None

    def remove_request(self, user_id: int) -> None:
        """
        Удаление запроса от пользователя с user_id
        :param user_id: id пользоваьеля
        """
        LOGGER.debug('Remove requests; user_id=%s', user_id)
        if user_id in self._requests:
            self._requests.pop(user_id)

    def get_admins(self) -> List[User]:
        """
        Получение списка администраторов
        :return: список администраторов
        """
        LOGGER.debug('Get admins')
        admins = [u for u in self._users.values()
                  if u.get_status() == 'admin']
        return admins

    def get_user(self, user_id: int) -> User:
        """
        Получение пользователя по id
        :param user_id: id пользователя
        :return: Если пользователь с user_id существует, то
                 возвращается его
                 Если не существует, то None
        """
        LOGGER.debug('Get user; user_id=%s', user_id)
        if user_id in self._users:
            return self._users[user_id]
        return None

    def update_rights(self, user_id: int, date: str) -> None:
        """
        Обновление прав пользователя.
        Права пользователя с user_id дополняются новыми или
        изменяется дата уже имеющихся
        :param user_id: id пользователя
        :param date: дата окончания прав
        """
        LOGGER.debug('Set rights; user_id=%s, date=%s', user_id, date)
        if user_id in self._users and user_id in self._requests:
            folders = [f for f in self._requests[user_id].get_dirs()]
            rights = {f: date for f in folders}

            self._users[user_id].update_rights(rights)
            self._db.set_rights(user_id, rights)

    def remove_rights(self, user_id: int, rights: List[str] = None):
        """
        Удаление прав поьзователя с user_id
        :param user_id: id пользователя
        :param rights: словарь прав
        """
        LOGGER.debug('Remove rights; user_id=%s, rights=%s', user_id, rights)
        if user_id not in self._users:
            LOGGER.debug('Remove rights; user_id=%s, user not found', user_id)
            return
        user = self._users[user_id]
        user.remove_rights(rights)
        self._db.remove_rights(user.get_user_id(), rights)

    def set_list_admins(self, list_admins: str) -> None:
        """
        Установка списка администраторов
        :param list_admins: строка с username администраторов
        """
        LOGGER.debug('Set list admins; list_admins=%s', list_admins)
        for username in list_admins.replace(' ', '').split(','):
            self._list_admins.append(username)

        LOGGER.debug('Set list admins; self._list_admins=%s', self._list_admins)
        for user_id in self._users:
            LOGGER.debug('Set list admins; user=%s', self._users[user_id])
            username = self._users[user_id].get_username()
            if username in self._list_admins:
                LOGGER.debug('Set list admins; Set status admin, user=%s', self._users[user_id])
                self._users[user_id].set_status('admin')

    def clear_dirs(self, dirs: List[str]) -> None:
        """
        Удаление всех файлов из заданных директорий за исключением самого нового
        :param dirs: список директорий
        """
        LOGGER.debug('Clear dirs; dirs=%s', dirs)

        format_files = ['txt', 'jpg']
        format_date = '%Y.%m.%d %H.%M.%S'
        base_pattern = r'\d{4}.\d{2}.\d{2}.\d{2}.\d{2}.\d{2}'
        list_pattern = ['%s.%s' % (base_pattern, ff) for ff in format_files]
        list_path_dirs = ['%s/%s' % (self._folder, folder) for folder in dirs]
        remove_files = []

        for folder in list_path_dirs:

            if not os.path.isdir(folder):
                LOGGER.error('Clear dirs; directory=%s not found', folder)
                continue

            for pattern, current_format in zip(list_pattern, format_files):
                list_files = []

                for file in os.listdir(folder):
                    if re.fullmatch(pattern, file):
                        list_files.append(file)

                if not list_files:
                    break

                list_date = []
                for file in list_files:
                    list_date.append(datetime.datetime.strptime('.'.join(file.split('.')[:-1]), format_date))

                list_date.sort()

                for date in list_date[:-1]:
                    path = '%s/%s.%s' % (folder, date.strftime(format_date), current_format)
                    remove_files.append(path)

        LOGGER.debug('Clear dirs; remove_files=%s', remove_files)

        for file in remove_files:
            if not os.path.isfile(file):
                LOGGER.debug('Clear dirs; file=%s not found', file)
                continue
            LOGGER.debug('Clear dirs; remove_file=%s', file)
            os.remove(file)

    def log_command(self, user_id: int, command: str) -> None:
        """
        Сохранение команды поьзователя с user_id
        :param user_id: id пользователя
        :param command: команда
        """
        LOGGER.debug('Log command; user_id=%s, command=%s', user_id, command)

        if user_id not in self._users:
            LOGGER.debug('Log command; user_id=%s not found', user_id)
            return

        user = self._users[user_id]
        h_file = '%s/%s.txt' % (self._history_dir, user.get_user_id())

        with open(h_file, 'a+') as file:
            file.seek(0)
            lines = file.readlines()

            if len(lines) >= 1000:
                file.seek(0)
                file.truncate()
            elif lines:
                file.write("\n")

            file.write(command)

    def get_last_command(self, user_id: int, count: int) -> List[str]:
        """
        Возвращает помледние count комманд пользователя с user_id
        :param user_id: id пользователя
        :param count: количество команд с конца
        :return: список последних count команд
        """
        LOGGER.debug('Get last command; user_id=%s, count=%s', user_id, count)

        if count < 0:
            LOGGER.debug('Get last command; count=%s < 0', count)
            return None

        if user_id not in self._users:
            LOGGER.debug('Get last command; user_id=%s not found', user_id)
            return None

        user = self._users[user_id]
        h_file = '%s/%s.txt' % (self._history_dir, user.get_user_id())

        LOGGER.debug('h_file=%s', h_file)

        if not os.path.isfile(h_file):
            LOGGER.debug('Get last command; Dir not exist=%s', user.get_history_file())
            return None

        last_command = []

        with open(h_file, 'a+') as file:
            file.seek(0)
            lines = file.readlines()
            last_command = lines[:count]

        last_command = [com.replace('\n', '') for com in last_command]

        return last_command

    def _init_history_files(self):
        """
        Инициализация файлов с историей команд пользователей
        """
        LOGGER.debug('Init history files')

        for user in self._users:
            history_file = '%s/%s.txt' % \
                           (self._history_dir, self._users[user].get_user_id())

            if not os.path.isfile(history_file):
                with open(history_file, 'w'):
                    pass
