"""
    Module for automatic determination of parking spaces
    based on the limiting frames of cars currently in the parking lot
    [   Used if the location of the camera allows you to compare
        the bounding frames of the car as an object
        and the area of the parking space   ]

                    |_|_|_|_|_|
                    |_|_|_|_|_|

                         .
                       camera
"""
import os
import torch

# Classes: default value [1,2,3,5,7] - all vehicles
# 1 - bicycle       # 2 - cars        # 3 - motorcycle
# 5 - bus           # 7 - truck
# https://tech.amikelive.com/node-718/what-object-categories-labels-are-in-coco-dataset/
default_classes = [1, 2, 3, 5, 7]


def apply_model(image):
    """
        Main function, in which the YOLO model call is implemented
        to determine the bounding boxes for all current cars in the image

        To determine the vehicles in the image used the best YOLO model
        to avoid the uncorrectness of determining cars
        with the overlap of other vehicles

        Argument:
            image - the image for which you want to determine parking spaces
    """
    # model from local repository [YOLOv5 6.0]
    model = torch.hub.load('./src', 'custom', 'model_weights/yolov5x.pt', source='local')
    result = model(image)

    objects = result.pandas().xyxy[0].values
    filename = os.path.splitext(os.path.basename(image))[0].split('_')[0]
    output_file = 'markup/' + filename + '_auto-coordinates.yml'
    with open(output_file, 'w', encoding='UTF-8') as file:
        # counter of parking spaces
        ids = 1
        for object_values in objects:
            if object_values[5] == 2:  # cars in default_classes
                temp = calculation_coordinates(object_values)
                added_to_yaml(file, temp, ids)
                ids += 1


def calculation_coordinates(object_values):
    """
        Function forms the coordinates of the object in the system (x,y) on the plane

        Argument:
            object - an object values of the DataFrame format
            obtained as a result of conversion from pandas
    """
    coordinates = [(int(object_values[0]), int(object_values[1])),
                   (int(object_values[2]), int(object_values[1])),
                   (int(object_values[2]), int(object_values[3])),
                   (int(object_values[0]), int(object_values[3]))]
    return coordinates


def added_to_yaml(file, coordinates, ids):
    """
        Function of adding a string with coordinates
        to the yml file with the necessary markup of the note

        Arguments:
            file - output file .yml
            coordinates - coordinates of the parking space
                        (obtained using the function @calculation_coordinates)
            ids - serial number of the parking space
    """
    file.write("-\n          id: " + str(ids) + "\n          markup_coordinates: [" +
               "[" + str(coordinates[0][0]) + "," + str(coordinates[0][1]) + "]," +
               "[" + str(coordinates[1][0]) + "," + str(coordinates[1][1]) + "]," +
               "[" + str(coordinates[2][0]) + "," + str(coordinates[2][1]) + "]," +
               "[" + str(coordinates[3][0]) + "," + str(coordinates[3][1]) + "]]\n")
