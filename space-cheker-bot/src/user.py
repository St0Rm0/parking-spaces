"""
Класс User.
This exports:
 - User
"""
from typing import Dict, List
from bot_logger import get_logger

LOGGER = get_logger(__name__)


class User:
    """
    Класс User.
    Предназначен для хранения данных о пользователе
    """
    def __init__(self, user_id,
                 chat_id,
                 first_name,
                 username,
                 status: str = None,
                 rights: Dict[str, str] = None,
                 history_file: str = ''):
        """
        Конструктор класса User
        :param user_id: id пользователя
        :param chat_id: id чата пользователя
        :param first_name: имя пользователя
        :param username: имя пользователя в формате "@..."
        :param status: статус пользователя (user или admin)
        :param rights: словарь прав доступа к диресториям
            :key название директории
            :var дата истечения прав
        """
        LOGGER.info('create user')
        self._user_id = user_id
        self._chat_id = chat_id
        self._first_name = first_name
        self._username = username
        self._status = status or 'user'
        self._rights = rights or {}
        self._history_file = history_file

    def get_user_id(self) -> int:
        """
        Метод, возвращает id пользователя
        :return: id пользователя
        """
        LOGGER.debug('get_user_id')
        return self._user_id

    def get_rights(self) -> Dict[str, str]:
        """
        Метод, возвращает словарь прав доступа пользователя
        :return: словарь прав
        """
        LOGGER.debug('get_rights')
        return self._rights

    def set_rights(self, rights: Dict[str, str]) -> None:
        """
        Метод, устанавливает права доступа пользователя
        :param rights: словарь прав
            :key название директории
            :var дата истечения прав
        """
        self._rights = rights

    def update_rights(self, rights: Dict[str, str]) -> None:
        """
        Метод, обновляет права доступа пользователя
        :param rights: словарь прав
            :key название директории
            :var дата истечения прав
        """
        for rght in rights:
            self._rights[rght] = rights[rght]

    def remove_rights(self, rights: List[str] = None):
        """
        Метод, удаляет права доступа пользователя
        :param rights: список директорий, доступ к которым нужно удалить
            Если rights is None, то удаляются все права
        """
        if rights is None:
            self._rights.clear()
        else:
            for folder in rights:
                if folder in self._rights:
                    self._rights.pop(folder)

    def get_first_name(self) -> str:
        """
        Метод, возвращает имя пользователя
        :return: имя пользователя
        """
        LOGGER.debug('get_first_name')
        return self._first_name

    def get_username(self) -> str:
        """
        Метод, возвращает имя пользователя формат "@..."
        :return: имя пользователя
        """
        LOGGER.debug('get_user_name')
        return self._username

    def set_status(self, status: str) -> None:
        """
        Метод, устанавливает статус пользователя
        :param status: новый статус пользователя
        """
        self._status = status

    def get_status(self) -> str:
        """
        Метод, возвращает статус пользователя
        :return: статус пользователя
        """
        return self._status

    def get_chat_id(self) -> int:
        """
        Метод, возвращает id чата пользователя
        :return: id чата
        """
        return self._chat_id

    def set_history_file(self, history_file: str) -> None:
        """
        Метод устанавливает название файла логирования комманд
        :param history_file: название файла
        """
        self._history_file = history_file

    def get_history_file(self) -> str:
        """
        Метод возвращает название файла логгирования
        :return: название файла логирования
        """
        return self._history_file

    def __str__(self):
        """
        Метод, для строкового представления пользователя
        :return: некоторые данные пользователя в строковам виде
        """
        return '%s (@%s)' % (self._first_name, self._username)

    def __repr__(self):
        """
        Метод, для строкового представления пользователя
        :return: некоторые данные пользователя в строковам виде
        """
        return '%s (@%s): %s' % (
            self._first_name, self._username, list(self._rights.keys()))
