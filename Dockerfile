FROM python:3.10-slim
# look at https://github.com/anibali/docker-pytorch and
# https://hub.docker.com/r/anibali/pytorch
#FROM anibali/pytorch:1.13.0-nocuda-ubuntu22.04
#FROM anibali/pytorch:1.13.0-cuda11.8-ubuntu22.04
#FROM anibali/pytorch:2.0.0-cuda11.8-ubuntu22.04

RUN apt-get update && apt-get install -y \
    bzip2 \
    ca-certificates \
    curl \
    ffmpeg \
    gcc \
    libsm6 \
    libx11-6 \
    libxext6 \
    sudo \
 && rm -rf /var/lib/apt/lists/*

COPY requirements.txt requirements.txt

RUN  pip3 install --no-cache-dir --upgrade pip

RUN pip3 install --no-cache-dir --default-timeout=1000 -r requirements.txt

RUN mkdir /parking-spaces

WORKDIR /parking-spaces

COPY src /parking-spaces/src

COPY requirements.txt /parking-spaces/requirements.txt

# change in docker fixed a library error [read more in repository]
RUN cp src/upsampling.py /usr/local/lib/python3.10/site-packages/torch/nn/modules/

ENTRYPOINT ["python3", "src/detect_customize.py"]
