import pandas as pd
import matplotlib.pyplot as plt


def histogram_template(statics):
    hours = list(range(24))
    # 5 - 10
    morning_data = statics[5:12]
    # 11 - 16
    day_data = statics[11:17]
    # 17 - 23
    evening_data = statics[17:24]
    night_data = statics[22:] + statics[:4]

    mean_cars_per_hour = pd.Series(statics).groupby(hours).mean()
    morning_cars = pd.Series(morning_data).groupby(hours[5:12]).mean()
    day_cars = pd.Series(day_data).groupby(hours[11:17]).mean()
    evening_cars = pd.Series(evening_data).groupby(hours[17:24]).mean()
    plt.bar(hours, mean_cars_per_hour, align='center')
    plt.bar(hours[5:12], morning_cars, align='center')
    plt.bar(hours[11:17], day_cars, align='center')
    plt.bar(hours[17:24], evening_cars, align='center')

    max_cars = max(statics)
    min_cars = min(statics)
    max_cars_text = list(range(2))
    min_cars_text = list(range(2))
    for rect in plt.gca().patches:
        if rect.get_height() == max_cars:
            max_cars_bar_geo = rect.get_xy()
            max_cars_text[1] = max_cars_bar_geo[1] + rect.get_height()
            max_cars_text[0] = max_cars_bar_geo[0] + rect.get_width() / 2
        if rect.get_height() == min_cars:
            min_cars_bar_geo = rect.get_xy()
            min_cars_text[1] = min_cars_bar_geo[1] + rect.get_height()
            min_cars_text[0] = min_cars_bar_geo[0] + rect.get_width() / 2
    if min_cars != -1:
        plt.annotate(f'{min_cars:.0f}', xy=min_cars_text,
                     xytext=min_cars_text,
                     ha='center', va='bottom', fontsize=10, color='black')
    if max_cars != -1:
        plt.annotate(f'{max_cars:.0f}', xy=max_cars_text,
                     xytext=max_cars_text,
                     ha='center', va='bottom', fontsize=10, color='black')

    plot_hours = [0, 4, 5, 10, 11, 15, 16, 23]
    plt.xticks(plot_hours)


def histogram_occupied_places(parking_lot_statics, max_count, path_to_save):
    # statics = [17, 22, 22, 23, 24, 24, 28, 33, 37, 40,
    #           38, 30, 28, 28, 32, 39, 42, 41, 34, 30,
    #           24, 24, 18, 18]
    histogram_template(parking_lot_statics)
    plt.ylim(0, max_count + 10)
    plt.xlabel('Часы')
    plt.ylabel('Среднее количество машин')
    plt.title('Среднее количество машин на парковке по часам')
    plt.legend(['Night', 'Morning',
                'Day', 'Evening'])

    plt.savefig(path_to_save + 'occupied_histogram.jpg')
    plt.clf()


def histogram_avaliable_places(parking_lot_statics, path_to_save):
    # statics = [23, 18, 18, 17, 16, 16, 12, 8, 5, 3,
    #           5, 9, 11, 12, 9, 7, 1, 2, 6, 10,
    #           16, 16, 22, 22]
    histogram_template(parking_lot_statics)
    plt.ylim(0, max(parking_lot_statics)+5)
    plt.xlabel('Часы')
    plt.ylabel('Среднее количество свободных мест')
    plt.title('Среднее количество свободных мест на парковке по часам')
    plt.legend(['Night', 'Morning',
                'Day', 'Evening'])
    plt.savefig(path_to_save + 'avaliable_histogram.jpg')
    plt.clf()


def average_avaliable_places(parking_lot_statics):
    count, total = 0, 0
    for data in parking_lot_statics:
        if data != -1:
            count += 1
            total += data
    if count > 0:
        return total // count
    else:
        return 0
