'''
    Main module for
'''

# pylint: disable = E1101

import argparse
import time
from pathlib import Path

import datetime
import os

import cv2
import numpy as np
import torch
import torch.backends.cudnn as cudnn

import bot_directories
import statistic_analysis

from models.experimental import attempt_load
from parking_lot_markings import load_data_from_markups
from utils.datasets import LoadStreams, LoadImages
from utils.general import check_img_size, check_requirements, \
    check_imshow, non_max_suppression, apply_classifier, \
    scale_coords, xyxy2xywh, strip_optimizer, set_logging, increment_path, save_one_box
from utils.plots import colors, plot_one_box, show_roi_area
from utils.torch_utils import select_device, load_classifier, time_synchronized

count_places = 0
count_avaliable_places = 0
parking_lot_statics = [-1] * 24
parking_lot_avaliable = [-1] * 24


def detect(opt):
    global c

    source, weights, view_img, save_txt, imgsz = \
        opt.source, opt.weights, opt.view_img, opt.save_txt, opt.img_size
    loop_time, no_loop_save = opt.loop_time, opt.no_loop_save
    save_img = opt.save_result and not source.endswith('.txt')  # save inference images
    webcam = source.isnumeric() or source.endswith('.txt') or source.lower().startswith(
        ('rtsp://', 'rtmp://', 'http://', 'https://'))

    # Directories
    save_dir = increment_path(Path(opt.project) / opt.name, exist_ok=opt.exist_ok)  # increment run
    if save_img:
        (save_dir / 'labels' if save_txt else save_dir).mkdir(parents=True,
                                                              exist_ok=True)  # make dir
    if not no_loop_save:
        id_dir = bot_directories.directory_manager()
    save_tg_dir = 'space-cheker-bot/parking/'

    # Initialize
    set_logging()
    device = select_device(opt.device)
    half = device.type != 'cpu'  # half precision only supported on CUDA

    # Load model
    model = attempt_load(weights, map_location=device)  # load FP32 model
    stride = int(model.stride.max())  # model stride
    imgsz = check_img_size(imgsz, s=stride)  # check img_size
    names = model.module.names if hasattr(model, 'module') else model.names  # get class names
    if half:
        model.half()  # to FP16

    # Second-stage classifier
    classify = False
    if classify:
        modelc = load_classifier(name='resnet101', n=2)  # initialize
        modelc.load_state_dict(torch.load('weights/resnet101.pt',
                                          map_location=device)['model']).to(device).eval()

    # Set Dataloader
    vid_path, vid_writer = None, None
    if webcam:
        view_img = check_imshow()
        cudnn.benchmark = True  # set True to speed up constant image size inference
        dataset = LoadStreams(source, img_size=imgsz, stride=stride)
    else:
        dataset = LoadImages(source, img_size=imgsz, stride=stride)

    # Run inference
    if device.type != 'cpu':
        model(torch.zeros(1, 3, imgsz, imgsz).to(device)
              .type_as(next(model.parameters())))  # run once
    t0 = time.time()

    temp_time = time.time()

    for path, img, im0s, vid_cap in dataset:
        img = torch.from_numpy(img).to(device)
        img = img.half() if half else img.float()  # uint8 to fp16/32
        img /= 255.0  # 0 - 255 to 0.0 - 1.0
        if img.ndimension() == 3:
            img = img.unsqueeze(0)

        # Inference
        t1 = time_synchronized()
        pred = model(img, augment=opt.augment)[0]

        # Classes: default value [1,2,3,5,7] - all vehicles
        # 1 - bicycle       # 2 - cars        # 3 - motorcycle
        # 5 - bus           # 7 - truck
        # https://tech.amikelive.com/node-718/what-object-categories-labels-are-in-coco-dataset/

        # Apply NMS
        pred = non_max_suppression(pred, opt.conf_thres,
                                   opt.iou_thres, classes=opt.classes, agnostic=opt.agnostic_nms)
        t2 = time_synchronized()

        # Apply Classifier
        if classify:
            pred = apply_classifier(pred, modelc, img, im0s)

        # Process detections
        for i, det in enumerate(pred):  # detections per image

            if webcam:  # batch_size >= 1
                p, s, im0, frame = path[i], '%g: ' % i, im0s[i].copy(), dataset.count
            else:
                p, s, im0, frame = path, '', im0s.copy(), getattr(dataset, 'frame', 0)

            p = Path(p)  # to Path
            save_path = str(save_dir / p.name)  # img.jpg
            txt_path = str(save_dir / 'labels' / p.stem) + \
                       ('' if dataset.mode == 'image' else f'_{frame}')  # img.txt
            s += '%gx%g ' % img.shape[2:]  # print string
            gn = torch.tensor(im0.shape)[[1, 0, 1, 0]]  # normalization gain whwh
            global count_places, count_avaliable_places
            if len(det):
                # Rescale boxes from img_size to im0 size
                det[:, :4] = scale_coords(img.shape[2:], det[:, :4], im0.shape).round()

                # Print results
                for c in det[:, -1].unique():
                    n = (det[:, -1] == c).sum()  # detections per class
                    s += f"{n} {names[int(c)]}{'s' * (n > 1)}, "  # add to string

                total_det = []
                # Write results

                for *xyxy, conf, cls in reversed(det):
                    if save_txt:  # Write to file
                        xywh = (xyxy2xywh(torch.tensor(xyxy).view(1, 4)) / gn) \
                            .view(-1).tolist()  # normalized xywh
                        line = (cls, *xywh, conf) if opt.save_conf else (cls, *xywh)  # label format
                        with open(txt_path + '.txt', 'a') as f:
                            f.write(('%g ' * len(line)).rstrip() % line + '\n')

                    # opt.save_crop=True
                    if save_img or opt.save_crop or view_img:  # Add bbox to image
                        c = int(cls)  # integer class
                        #                        label = None if opt.hide_labels else (names[c] if opt.hide_conf
                        #                                                              else f'{names[c]} {conf:.2f}')

                        plot_one_box(xyxy, im0, color=colors(c, True),
                                     line_thickness=opt.line_thickness)
                        total_det.append([int(xyxy[0]), int(xyxy[1]), int(xyxy[2]), int(xyxy[3])])
                        if opt.save_crop:
                            save_one_box(xyxy, im0s, file=save_dir /
                                                          'crops' / names[c] / f'{p.stem}.jpg', BGR=True)
                # print(len(Total_det))
                # total spaces, space avaliable, total cars
                count_places, count_avaliable_places, \
                    count_cars = show_roi_area(im0, total_det,
                                               source, line_thickness=opt.line_thickness)

            else:
                markup_file = 'markup/' + \
                              str(os.path.splitext(os.path.basename(source))[0]) \
                              + '_coordinates.yml'
                markup = load_data_from_markups(markup_file)
                count_places = count_avaliable_places = str(len(markup))
                cv2.drawContours(im0, [np.array(markup[i])], contourIdx=-1, color=(0, 255, 0),
                                 thickness=2, lineType=cv2.LINE_8)
                msg = 'Total spaces:' + count_places + '  Spaces available:' + count_avaliable_places
                moments = cv2.moments(np.array(markup[i]))
                center = (int(moments["m10"] / moments["m00"]) - 3,
                          int(moments["m01"] / moments["m00"]) + 3)
                tl = opt.line_thickness or round(0.002 * (im0.shape[0] + im0.shape[1]) / 2) + 1
                tf = max(tl - 1, 1)

                cv2.putText(im0, 'Space ', (center[0] - 10, center[1] - 10),
                            0, tl / 3, (0, 255, 0), thickness=tf,
                            lineType=cv2.LINE_AA)
                cv2.putText(im0, 'Available ', (center[0] - 15, center[1]),
                            0, tl / 3, (0, 255, 0), thickness=tf,
                            lineType=cv2.LINE_AA)
                cv2.putText(im0, msg, (10, 35), 0, 1, (0, 255, 0),
                            thickness=3, lineType=cv2.LINE_AA)

            # Print time (inference + NMS)
            # print(f'{s}Done. ({t2 - t1:.3f}s)')
            # Stream results
            if view_img:
                cv2.imshow(str(p), im0)
                cv2.waitKey(1)  # 1 millisecond

            # Save parking space data to the telegram directory
            if not no_loop_save:
                if time.time() > temp_time + loop_time:
                    temp_time = time.time()
                    dt_now = datetime.datetime.now()
                    str_dt = dt_now.strftime("%Y.%m.%d %H.%M.%S")
                    path = save_tg_dir + id_dir + "/" + str_dt + ".jpg"
                    if parking_lot_statics[dt_now.hour] != -1:
                        parking_lot_statics[dt_now.hour] = -1 * ((parking_lot_statics[dt_now.hour]
                                                            + (count_places - count_avaliable_places)) // 2) // 1 * -1
                    else:
                        parking_lot_statics[dt_now.hour] = count_places
                    parking_lot_avaliable[dt_now.hour] = (parking_lot_avaliable[dt_now.hour]
                                                          + count_avaliable_places // 2)
                    statistic_analysis.histogram_avaliable_places(parking_lot_avaliable, save_tg_dir + id_dir + "/")
                    statistic_analysis.histogram_occupied_places(parking_lot_statics, count_places,
                                                                 save_tg_dir + id_dir + "/")
                    mask = 'histogram.jpg'
                    for file in os.listdir(save_tg_dir + id_dir + "/"):
                        if not file.endswith(mask):
                            os.remove(save_tg_dir + id_dir + "/" + file)

                    with open(save_tg_dir + id_dir + "/" + str_dt + ".txt", "w+",
                              encoding='UTF-8') as output_file:
                        output_file.write(" ## Дата последнего обновления   " + str_dt
                                          + "\nОтслеживается парковочных мест: " + str(count_places)
                                          + "\nДоступно парковочных мест: " +
                                          str(count_avaliable_places))
                        output_file.close()

                    with open(save_tg_dir + id_dir + "/analysis_histogram.txt", "w+",
                              encoding='UTF-8') as analysis_file:
                        analysis_file.write(" ## Дата последнего обновления   " + str_dt
                                            + "\nМаксимальное количество занятых мест [усреднено в рамках часа]\n - " + str(max(parking_lot_statics))
                                            + " в период с " + str(parking_lot_statics.index(max(parking_lot_statics))) + " до "
                                            + str(parking_lot_statics.index(max(parking_lot_statics))+1)
                                            + "\nМаксимальное количество свободных мест [усреднено в рамках часа]\n - " + str(max(parking_lot_avaliable))
                                            + " в период с " + str(parking_lot_avaliable.index(max(parking_lot_avaliable))) + " до "
                                            + str(parking_lot_avaliable.index(max(parking_lot_avaliable)) + 1)
                                            + "\nСреднее количество свободных мест на протяжении всего дня - "
                                            + str(statistic_analysis.average_avaliable_places(parking_lot_avaliable)))
                        analysis_file.close()
                    cv2.imwrite(save_tg_dir + id_dir + "/" + str_dt + ".jpg", im0)

            # Save results (image with detections)
            if save_img:
                if dataset.mode == 'image':
                    cv2.imwrite(save_path, im0)
                else:  # 'video' or 'stream'
                    if vid_path != save_path:  # new video
                        vid_path = save_path
                        if isinstance(vid_writer, cv2.VideoWriter):
                            vid_writer.release()  # release previous video writer
                        if vid_cap:  # video
                            fps = vid_cap.get(cv2.CAP_PROP_FPS)
                            w = int(vid_cap.get(cv2.CAP_PROP_FRAME_WIDTH))
                            h = int(vid_cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
                        else:  # stream
                            fps, w, h = 30, im0.shape[1], im0.shape[0]
                            save_path += '.mp4'
                        vid_writer = cv2.VideoWriter(save_path,
                                                     cv2.VideoWriter_fourcc(*'mp4v'), fps, (w, h))
                    vid_writer.write(im0)

    if save_txt or save_img:
        s = f"\n{len(list(save_dir.glob('labels/*.txt')))} " \
            f"labels saved to {save_dir / 'labels'}" if save_txt else ''
        print(f"Results saved to {save_dir}{s}")

    #print(f'Done. ({time.time() - t0:.3f}s)')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--weights', nargs='+', type=str, default='model_weights/yolov5n.pt',
                        help='model.pt path(s)')
    parser.add_argument('--source', type=str,
                        default=0,
                        help='source')  # file/folder, 0 for webcam
    parser.add_argument('--img-size', type=int, default=640, help='inference size (pixels)')
    parser.add_argument('--conf-thres', type=float, default=0.25,
                        help='object confidence threshold')
    parser.add_argument('--iou-thres', type=float, default=0.45, help='IOU threshold for NMS')
    parser.add_argument('--device', default='', help='cuda device, i.e. 0 or 0,1,2,3 or cpu')
    parser.add_argument('--view-img', action='store_true', help='display results')
    parser.add_argument('--save-txt', action='store_true', help='save results to *.txt')
    parser.add_argument('--mark-parking', action='store_true', help='mark the parking space')
    parser.add_argument('--save-conf', action='store_true',
                        help='save confidences in --save-txt labels')
    parser.add_argument('--save-crop', action='store_true', help='save cropped prediction boxes')
    parser.add_argument('--save-result', action='store_true', help='do not save images/videos')

    parser.add_argument('--classes', default=[1, 2, 3, 5, 7], nargs='+', type=int,
                        help='filter by class: --class 0, or --class 0 2 3')
    parser.add_argument('--agnostic-nms', action='store_true', help='class-agnostic NMS')
    parser.add_argument('--augment', action='store_true', help='augmented inference')
    parser.add_argument('--update', action='store_true', help='update all models')
    parser.add_argument('--project', default='runs/detect', help='save results to project/name')
    parser.add_argument('--name', default='exp', help='save results to project/name')
    parser.add_argument('--exist-ok', action='store_true',
                        help='existing project/name ok, do not increment')
    parser.add_argument('--line-thickness', default=1, type=int,
                        help='bounding box thickness (pixels)')
    parser.add_argument('--hide-labels', action='store_true', help='hide labels')
    parser.add_argument('--hide-conf', action='store_true', help='hide confidences')

    parser.add_argument('--loop-time', default=10, type=int,
                        help='time of updating the parking space data in '
                             'telegram bot')
    parser.add_argument('--no-loop-save', action='store_true',
                        help='do not save parking space data to the telegram bot directory')
    # parser.add_argument('--save-dir', default=False, action='store_true', help='hide confidences')
    opt = parser.parse_args()
    
    if opt.mark_parking:
        # Absolutely wrong function call, it is obvious that you need
        # to use the connection of modules. However,
        # this option is currently not possible due to the fact that
        # cv2.waitKey() cannot be correctly executed by
        # cv2.imshow in the presence of the matplotlib library.
        # Moreover, even such a call does not allow using the
        # keyboard after the markup stage. Priority is the separate use of the markup function.
        # More there: https://github.com/fourMs/MGT-python/issues/200
        os.system('python3 src/parking_lot_markings.py ' + opt.source)

    check_requirements(exclude=('tensorboard', 'pycocotools', 'thop'))
    with torch.no_grad():
        if opt.update:  # update all models (to fix SourceChangeWarning)
            for opt.weights in ['yolov5s.pt', 'yolov5m.pt', 'yolov5l.pt', 'yolov5x.pt']:
                detect(opt=opt)
                strip_optimizer(opt.weights)
        else:
            detect(opt=opt)
