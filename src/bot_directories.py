import os


def directory_manager(path='space-cheker-bot/parking/'):
    current_directory_number = 0
    it = 1
    if not os.path.exists(path):
            os.mkdir(path, 0o755)
    while current_directory_number == 0:
        temp_path = path + str(it)
        if not os.path.exists(temp_path):
            os.mkdir(temp_path, 0o755)
            current_directory_number = it
            print("Information message: directory '" + str(it) +
                  "' was created in the project directory and selected as working directory for current source")
            break
        elif os.path.exists(temp_path) and \
                len([name for name in os.listdir(temp_path) if os.path.isfile(os.path.join(temp_path, name))]) <= 1:
            current_directory_number = it
            print("Information message: directory '" + str(it) +
                  "' was selected as working directory for current videosource")
            break
        else:
            it += 1

    return str(current_directory_number)

