"""
Модуль database
This exports:
 - DataBase
"""
from typing import List, Dict
from user import User
import psycopg2
from bot_logger import get_logger


LOGGER = get_logger(__name__)

USERS_COLUMNS_NAME = [
    "id",
    "chat_id",
    "first_name",
    "username",
    "status",
]

RIGHTS_COLUMN_NAME = [
    'id',
    'directory',
    'date',
    'user_id',
]


class DataBase:
    """
    Запись и извлечение данных из базы
    """
    TABLE_USERS = 'users'
    TABLE_RIGHTS = 'rights'

    def __init__(self, host: str, pswd: str, user: str, db_name: str):
        """
        Конструктор базы данных.
        Установка основных параметров и создание таблиц
        :param host: database host address (defaults to UNIX socket if not provided)
        :param pswd: password used to authenticate
        :param user: user name used to authenticate
        :param db_name: the database name
        """
        LOGGER.info('Create database; host=%s, pswd=%s, user=%s, db_name=%s',
                    host, pswd, user, db_name)
        self._db_name = db_name
        self._user = user
        self._host = host
        self._pswd = pswd
        self._init_table()

    def _init_table(self) -> None:
        """
        Создание таблиц
        """
        LOGGER.info('Create table in database')
        try:
            LOGGER.info('Connection to database...')
            with psycopg2.connect(
                    dbname=self._db_name,
                    user=self._user,
                    password=self._pswd,
                    host=self._host) as conn:
                LOGGER.info('Connected')

                cursor = conn.cursor()
                LOGGER.debug('Create table if not exist...')

                request = f"CREATE TABLE if not exists {self.TABLE_USERS} (" \
                    f"{USERS_COLUMNS_NAME[0]} integer NOT NULL PRIMARY KEY," \
                    f"{USERS_COLUMNS_NAME[1]} integer NOT NULL," \
                    f"{USERS_COLUMNS_NAME[2]} text," \
                    f"{USERS_COLUMNS_NAME[3]} text," \
                    f"{USERS_COLUMNS_NAME[4]} text);"

                LOGGER.debug('Request=%s', request)

                cursor.execute(request)
                conn.commit()
                LOGGER.debug('Created')
                LOGGER.debug('Create table if not exist...')

                request = f"CREATE TABLE if not exists {self.TABLE_RIGHTS} (" \
                    f"{RIGHTS_COLUMN_NAME[0]} SERIAL," \
                    f"{RIGHTS_COLUMN_NAME[1]} text," \
                    f"{RIGHTS_COLUMN_NAME[2]} date," \
                    f"{RIGHTS_COLUMN_NAME[3]} integer NOT NULL " \
                    f"REFERENCES {self.TABLE_USERS}({USERS_COLUMNS_NAME[0]}));"

                LOGGER.debug('Request=%s', request)

                cursor.execute(request)
                conn.commit()

                LOGGER.debug('Created')

        except psycopg2.Error as ex:
            LOGGER.critical(ex)
        else:
            LOGGER.info('Database connection close')

    def get_users_list(self) -> List[User]:
        """
        Получение всех пользователей из базы данных
        :return: Список пользователей, если не произошло ошибок
                 в результате подключения к базе
                 Пустой список, если произошла ошибка
        """
        LOGGER.info('Get_users_list')
        users = []
        try:
            LOGGER.info('Connection to database...')
            with psycopg2.connect(
                    dbname=self._db_name,
                    user=self._user,
                    password=self._pswd,
                    host=self._host) as conn:
                LOGGER.info('Connected')

                cursor = conn.cursor()
                request = f'SELECT * FROM {self.TABLE_USERS};'
                LOGGER.info('Request=%s', request)

                cursor.execute(request)

                for row in cursor.fetchall():
                    user_id = row[0]
                    chat_id = row[1]
                    first_name = row[2]
                    username = row[3]
                    status = row[4]
                    rights = self._get_rights(user_id)
                    users.append(User(user_id, chat_id, first_name, username, status, rights))

        except psycopg2.Error as ex:
            LOGGER.critical(ex)
        else:
            LOGGER.info('Database connection close')

        return users

    def insert(self, user: User) -> None:
        """
        Добавление пользователя в базу
        :param user: Пользователь
        """
        LOGGER.info('Insert user')
        try:
            LOGGER.info('Connection to database...')
            with psycopg2.connect(
                    dbname=self._db_name,
                    user=self._user,
                    password=self._pswd,
                    host=self._host) as conn:
                LOGGER.info('Connected')
                cursor = conn.cursor()

                columns = ','.join(USERS_COLUMNS_NAME)
                request = f"INSERT INTO {self.TABLE_USERS} ({columns}) " \
                          f"VALUES(%s, %s, %s, %s, %s)"
                values = [
                    user.get_user_id(),
                    user.get_chat_id(),
                    user.get_first_name(),
                    user.get_username(),
                    user.get_status(),
                ]
                LOGGER.debug('Request=%s, values=%s', request, values)

                cursor.execute(request, values)
                conn.commit()

        except psycopg2.Error as ex:
            LOGGER.critical(ex)
        else:
            LOGGER.info('Database connection close')

    def set_rights(self, user_id: int, rights: Dict[str, str]) -> None:
        """
        Добавление прав доступа пользователя
        :param user_id: id пользователя
        :param rights: словарь прав
        """
        LOGGER.info('Set rights')
        try:
            LOGGER.info('Connection to database...')
            with psycopg2.connect(
                    dbname=self._db_name,
                    user=self._user,
                    password=self._pswd,
                    host=self._host) as conn:
                LOGGER.info('Connected')
                cursor = conn.cursor()

                for folder in rights:
                    request = f"SELECT * FROM {self.TABLE_RIGHTS} WHERE "\
                        f"{RIGHTS_COLUMN_NAME[3]} = {user_id} AND "\
                        f"{RIGHTS_COLUMN_NAME[1]} = '{folder}'"
                    LOGGER.debug('Request=%s', request)

                    cursor.execute(request)
                    if not cursor.fetchall():
                        columns = ','.join(RIGHTS_COLUMN_NAME[1:])
                        next_request = f"INSERT INTO {self.TABLE_RIGHTS} ({columns}) " \
                            f"VALUES ('{folder}', '{rights[folder]}', {user_id});"
                    else:
                        next_request = f"UPDATE {self.TABLE_RIGHTS} SET " \
                            f"{RIGHTS_COLUMN_NAME[2]} = '{rights[folder]}' " \
                            f"WHERE " \
                            f"{RIGHTS_COLUMN_NAME[3]} = {user_id} AND " \
                            f"{RIGHTS_COLUMN_NAME[1]} = '{folder}';"

                    LOGGER.debug('Next request=%s', next_request)
                    cursor.execute(next_request)
                    conn.commit()

        except psycopg2.Error as ex:
            LOGGER.critical(ex)
        else:
            LOGGER.info('Database connection close')

    def _get_rights(self, user_id: int) -> Dict[str, str]:
        """
        Получение всех прав доступа пользователя с user_id
        :param user_id: id пользователя
        :return: словарь прав
        """
        LOGGER.info('Get rights, user_id=%s', user_id)
        rights = {}
        try:
            LOGGER.info('Connection to database...')
            with psycopg2.connect(
                    dbname=self._db_name,
                    user=self._user,
                    password=self._pswd,
                    host=self._host) as conn:
                LOGGER.info('Connected')
                cursor = conn.cursor()
                request = f"SELECT * FROM {self.TABLE_RIGHTS} " \
                          f"WHERE {RIGHTS_COLUMN_NAME[3]} = {user_id}"
                LOGGER.debug('Request=%s', request)

                cursor.execute(request)

                data = cursor.fetchall()
                rights = {d[1]: d[2] for d in data}
                LOGGER.debug('Rights=%s', rights)

        except psycopg2.Error as ex:
            LOGGER.critical(ex)
        else:
            LOGGER.info('Database connection close')

        return rights

    def remove_rights(self, user_id: int, rights: List[str] = None) -> None:
        """
        Удаления прав доступа пользователя с user_id
        :param rights: список отзываемых прав
        :param user_id: id пользователя
        """
        LOGGER.info('Delete rights, user_id=%s, rights=%s', user_id, rights)
        try:
            LOGGER.info('Connection to database...')
            with psycopg2.connect(
                    dbname=self._db_name,
                    user=self._user,
                    password=self._pswd,
                    host=self._host) as conn:
                LOGGER.info('Connected')
                cursor = conn.cursor()

                if rights is not None:
                    for folder in rights:
                        request = f"DELETE FROM {self.TABLE_RIGHTS} " \
                                  f"WHERE " \
                                  f"{RIGHTS_COLUMN_NAME[3]} = {user_id} AND " \
                                  f"{RIGHTS_COLUMN_NAME[1]} = '{folder}';"
                        cursor.execute(request)
                        conn.commit()
                else:
                    request = f"DELETE FROM {self.TABLE_RIGHTS} " \
                              f"WHERE {RIGHTS_COLUMN_NAME[3]} = {user_id};"
                    cursor.execute(request)
                    conn.commit()

        except psycopg2.Error as ex:
            LOGGER.critical(ex)
        else:
            LOGGER.info('Database connection close')

    def __str__(self):
        return 'dbname=%s, user=%s, host=%s' % \
               (self._db_name, self._user, self._host)
